require "rails_helper"

RSpec.describe WeatherClient do
  # include ActiveSupport::Testing::TimeHelpers
  context '#make_api_call' do
    before do
      @address = Address.new({street: 'dumb', city: 'dumb', state: 'dumb', zip_code: '111111'})
    end

    it 'returns mocked data' do
      expect(described_class.new(@address).make_api_call).to eq WeatherClient::MOCK_RESPONSE
    end
  end
  context '#get_weather_info' do
    before do
      @address = Address.new({street: 'dumb', city: 'dumb', state: 'dumb', zip_code: '111111'})
      described_class.new(@address).get_weather_info
    end

    context 'address same zip code' do
      before do
        similar_address = Address.new({street: 'dumb1', city: 'dumb1', state: 'dumb1', zip_code: '111111'})
        @similar_client = described_class.new(similar_address)
      end
      it 'renders data from cache' do
        @similar_client.get_weather_info
        expect(@similar_client).not_to receive(:make_api_call)
      end
      it 'makes api call after 30 minutes' do
        travel_to Time.now + 31.minutes
        @similar_client.get_weather_info
        expect(@similar_client).to receive(:make_api_call)
      end
    end

    context 'address with zip code' do
      before do
        @different_address = Address.new({street: 'dumb', city: 'dumb', state: 'dumb', zip_code: '111112'})
      end
      it 'will not render data from cache' do
        different_client = described_class.new(@similar_address)
        different_client.get_weather_info
        expect(different_client).to receive(:make_api_call)
      end
    end
  end
end