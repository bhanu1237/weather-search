require "rails_helper"

RSpec.describe WeatherController, type: :controller do
  render_views
  context 'weather_info' do
    context 'empty page load' do
      it 'renders successfully' do
        get :weather_info
        expect(response).to be_successful
      end
    end

    context 'with errors in address' do
      it 'renders errors' do
        post :weather_info, params: { :address => { street: nil, city: nil, state: nil, zip_code: nil}}
        expect(response.body).to include "Street can&#39;t be blank,City can&#39;t be blank,State can&#39;t be blank,Zip code consists of 6 digits"
      end
    end
  end

  context 
end
