Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get '/', to: 'weather#weather_info'
  post '/', to: 'weather#weather_info', as: 'weather_info'
  get '/*all', to: 'weather#weather_info'
end
