class WeatherController < ApplicationController
  before_action :validate_address
  def weather_info
    weather_client = WeatherClient.new(@address)
    @weather_forecast, @cache_hit = weather_client.get_weather_info
  end

  protected

  def render_weather_info
    render template: 'weather/weather_info'
  end

  def validate_address
    render_weather_info and return if request.get?
    @address = Address.new(address_params)
    render_weather_info and return unless @address.valid?
  end
  def address_params
    params.require(:address).permit(:street, :city, :state, :zip_code)
    params[:address]
  end
end
