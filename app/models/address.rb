class Address
  attr_reader :street, :city, :state, :zip_code
  include ActiveModel::Validations
  validates :street, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :zip_code, format: { with: /\A\d{5}/, :message => 'consists of 6 digits' }

  def initialize(address_params)
    @street = address_params[:street]
    @city = address_params[:city]
    @state = address_params[:state]
    @zip_code = address_params[:zip_code]
  end
end