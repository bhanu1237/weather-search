require 'net/http'
class WeatherClient

  MOCK_RESPONSE = (1..5).map do |i|
    {
      "temp": 296.34 + i,
      "feels_like": 296.02 + i,
      "temp_min": 296.34 + i,
      "temp_max": 298.24 + i
    }
  end

  attr_reader :address
  def initialize(address)
    @address = address
  end

  def get_weather_info
    cache_hit = true
    weather_data = Rails.cache.fetch(address&.zip_code, expires_in: 30.minutes) do
      cache_hit = false
      make_api_call
    end
    [weather_data, cache_hit]
  end

  def make_api_call
    headers = {}
    headers['Authorization'] = 'Bearer NOTVALID'
    url = URI("https://api.openweathermap.org/data/2.5/weather?pincode=#{address&.zip_code}")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(url, headers)
    request["accept"] = 'application/json'
    response = http.request(request)
    if response.is_a?(Net::HTTPSuccess)
      response = JSON.parse(response.read_body)
    else
      response = MOCK_RESPONSE
    end
  end
end